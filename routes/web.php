<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/berita/list', 'BeritaController@list');
    
    Route::resource('berita','BeritaController');
    Route::resource('tag','TagController');
    Route::resource('kategori','KategoriController');
});


Auth::routes();



